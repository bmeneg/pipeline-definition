---
# gitlab-yaml-shellcheck: main=../cki_pipeline.yml

.common-export-vars: |
  # Export variables that we need to build kcidb schema
  if [ -z "${CI_JOB_YMD:-}" ] ; then
    export CI_JOB_YMD
    CI_JOB_YMD=$(date +%Y/%m/%d)
  fi
  export name git_url commit_hash branch message_id discovery_time
  export cki_pipeline_branch config_target

  KCIDB_CHECKOUT_ID="redhat:${CI_PIPELINE_ID}"
  KCIDB_BUILD_ID="${KCIDB_CHECKOUT_ID}_${ARCH_CONFIG:-}"
  if is_true "${DEBUG_KERNEL:-}"; then
    KCIDB_BUILD_ID="${KCIDB_BUILD_ID}_debug"
  fi
  export KCIDB_CHECKOUT_ID KCIDB_BUILD_ID

.before-setup: |
    # Set up the path for locally installed python executables and ccache.
    # NOTE(mhayden): This cannot be added in a pipeline environment variable
    # because it disrupts the paths that the gitlab-runner expects in the
    # container when it starts.
    export PATH=${HOME}/.local/bin:/usr/lib64/ccache:${PATH}

    # Apply OpenShift UID/GID workarounds.
    if [ -w '/etc/passwd' ]; then
      echo "cki:x:$(id -u):$(id -g):,,,:${HOME}:/bin/bash" >> /etc/passwd
    fi
    if [ -w '/etc/group' ]; then
      echo "cki:x:$(id -G | cut -d' ' -f 2)" >> /etc/group
    fi

    # Set up basic configuration items for git.
    git config --global user.name "CKI Pipeline"
    git config --global user.email "cki-project@gitlab.com"
    git config --global advice.detachedHead false

    # Ensure home directory is set.
    if [ -z "${HOME:-}" ]; then
      export HOME=/tmp
    fi
    echo "🏠 Home directory is set: ${HOME}"

    echo_yellow "📦 Container image:"
    echo "  GitLab: ${CI_JOB_IMAGE:-}"
    if [ -r /etc/cki-image ]; then
      echo "  CKI container image: $(cat /etc/cki-image)"
    fi

    if [ -v CI_PIPELINE_ID ]; then
      echo_yellow "🏗️ API URLs:"
      echo "  Pipeline: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}"
      echo "  Variables: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/variables"
      echo "  Job: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/jobs/${CI_JOB_ID}"
    fi

    # Configure curl
    cat >~/.curlrc <<EOF
      --location
      --silent
      --show-error
      --retry ${MAX_TRIES}
      --retry-delay ${MAX_WAIT}
      --connect-timeout 30
    EOF

    # Configure retries for wget
    cat >~/.wgetrc <<EOF
       tries = ${MAX_TRIES}
       # maximum wait time used for linear backoff
       waitretry = ${MAX_WAIT}
    EOF

    # Ensure the workdir and artifacts directory exist
    mkdir -p "${WORKDIR}" "${ARTIFACTS_DIR}"

    # Get CPU count related variables for compiling kernels, running xz etc.
    echo_yellow "💻 Determining CPU and job count"
    eval "$(get_cpu_count)"
    echo "  CPUs: ${CPUS_AVAILABLE}"
    echo "  job count: ${MAKE_JOBS}"

    # Retrieve artifacts from S3
    aws_s3_download_artifacts

    # software tarball is not available in prepare stage
    if [ "${CI_JOB_STAGE}" == 'prepare' ]; then
      setup_software
    else
      extract_software
    fi

    readarray trigger_vars < <("${VENV_PY3}" -m rcdefinition triggervars print | tr ' ' '\n'  )
    exp_trigger_vars="${trigger_vars[*]}"
    # We actually want to expand space-delimited trigger variable names below.
    # shellcheck disable=SC2086
    declare -p CI_JOB_YMD ${exp_trigger_vars} >> .variables 2>/dev/null || true

.after-cat-rc: |
  # Display RC file for easier debugging
  if [ -f "${RC_FILE}" ]; then
    cat "${RC_FILE}"
  fi

.after-helpful-links: |
  # Display a couple of relevant links
  if [ "${artifacts_mode}" = "s3" ]; then
    echo "This pipeline uses artifacts stored on S3:"
    echo_yellow " $(browsable_artifact_directory_url '')"
  fi
  if ! is_true "${retrigger}" && kcidb_edit_checkout get id > /dev/null 2>&1; then
    echo "More information about this pipeline can be found in DataWarehouse:"
    echo_yellow "  https://datawarehouse.cki-project.org/kcidb/checkouts/${KCIDB_CHECKOUT_ID}"
  fi
  if [ -n "${mr_id}" ] ; then
    echo "Check out CKI documentation if you have questions about next steps:"
    echo_yellow "  https://cki-project.org/docs/user_docs/gitlab-mr-testing/"
  fi

.common-before-script:
  - set -euo pipefail
  - !reference [.general-function-definitions]
  - !reference [.aws-function-definitions]
  - !reference [.kcidb-function-definitions]
  - !reference [.prepare-function-definitions]
  - !reference [.common-export-vars]
  - !reference [.before-setup]

.common-after-script-head:
  - !reference [.general-function-definitions]
  - !reference [.aws-function-definitions]
  - !reference [.kcidb-function-definitions]
  - !reference [.common-export-vars]

.common-after-script-tail:
  - !reference [.after-cat-rc]
  - !reference [.after-helpful-links]
